package com.rolep.javarush.simplecrud.dao;

import com.rolep.javarush.simplecrud.model.User;

import java.util.List;

/**
 * Created by rolep on 24/02/16.
 */
public interface UserDao {

    public void addUser(User user);

    public void editUser(User user);

    public List<User> getAllUsers();

    public User getUserById(int id);

    public void deleteUser(int id);

    public List<User> findUsers(int id, String name, int age);
}
