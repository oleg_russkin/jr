package com.rolep.javarush.simplecrud.dao;

import com.rolep.javarush.simplecrud.model.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by rolep on 24/02/16.
 */
@Repository
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    @Override
    public void editUser(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public List<User> getAllUsers() {
        return sessionFactory.getCurrentSession().createQuery("FROM User").list();
    }

    @Override
    public User getUserById(int id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    @Override
    public void deleteUser(int id) {
        User user = getUserById(id);
        if (user != null) {
            sessionFactory.getCurrentSession().delete(user);
        }
    }

    @Override
    public List<User> findUsers(int id, String name, int age) {
        Criteria criteria = sessionFactory.getCurrentSession().createCriteria(User.class);
        if (id > 0)
            criteria.add(Restrictions.eq("id", id));
        if (name != null && !name.equals(""))
            criteria.add(Restrictions.ilike("name", "%" + name + "%"));
        if (age > 0)
            criteria.add(Restrictions.eq("age", age));
        return criteria.list();
    }
}
