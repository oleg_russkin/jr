package com.rolep.javarush.simplecrud.controller;

import com.rolep.javarush.simplecrud.model.User;
import com.rolep.javarush.simplecrud.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * Created by rolep on 24/02/16.
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String redirect() {
        return "redirect:/users";
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String getUsers(Model model) {
        List<User> users = userService.getAllUsers();

        model.addAttribute("users", users);

        return "showallusers";
    }


    @RequestMapping(value = "/users/get", method = RequestMethod.GET)
    public String findUsers(@RequestParam(value = "id", required = false) String id, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "age", required = false) String age, Model model) {
        List<User> users;
        model.addAttribute("searchedId", id);
        model.addAttribute("searchedName", name);
        model.addAttribute("searchedAge", age);
        int sId;
        int sAge;
        if (id == null) {
            sId = 0;
        } else {
            try {
                sId = Integer.parseInt(id);
            } catch (NumberFormatException e) {
                sId = 0;
            }
        }
        if (name == null) name = "";
        if (age == null) sAge = 0;
        else {
            try {
                sAge = Integer.parseInt(age);
            } catch (NumberFormatException e) {
                sAge = 0;
            }
        }
        users = userService.findUsers(sId, name, sAge);

        model.addAttribute("users", users);

        return "showallusers";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.GET)
    public String initAdd(Model model) {
        model.addAttribute("userAttribute", new User());

        return "addinguser";
    }

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String add(@ModelAttribute("userAttribute") User user) {
        userService.addUser(user);
        return "addeduser";
    }

    @RequestMapping(value = "/users/delete", method = RequestMethod.GET)
    public String delete(@RequestParam(value = "id", required = true) int id, Model model) {
        userService.deleteUser(id);
        model.addAttribute("id", id);
        return "deleteduser";
    }

    @RequestMapping(value = "/users/edit", method = RequestMethod.GET)
    public String initEdit(@RequestParam(value = "id", required = true) int id, Model model) {
        model.addAttribute("userAttribute", userService.getUserById(id));
        return "editinguser";
    }

    @RequestMapping(value = "/users/edit", method = RequestMethod.POST)
    public String edit(@ModelAttribute("userAttribute") User user, @RequestParam(value = "id", required = true) int id, Model model) {
        userService.editUser(user);
        model.addAttribute("id", id);
        return "editeduser";
    }
}
