<%--
  Created by IntelliJ IDEA.
  User: rolep
  Date: 23/02/16
  Time: 23:40
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User DB</title>
</head>
<body>
<h1>User Database</h1>


<c:url var="searchUrl" value="/users/get"/>
<form method="GET" action="${searchUrl}">
    <table>
        <tr>
            <td>User ID:<input type="text" name="id"></td>
        </tr>

        <tr>
            <td>User name:<input type="text" name="name"></td>
        </tr>

        <tr>
            <td>User age:<input type="text" name="age"></td>
        </tr>
    </table>

    <input type="submit" value="Search"/>
</form>


<display:table name="users" requestURI="/users" pagesize="10" export="true">
    <display:column property="id" title="UserID" sortable="true" paramId="id"/>
    <display:column property="name" title="User name" sortable="true"/>
    <display:column property="age" title="Age" sortable="true"/>
    <display:column property="isAdmin" title="admin?" sortable="true"/>
    <display:column property="createdDate" title="Date of creation" sortable="true"/>
    <display:column href="${requestScope['javax.servlet.forward.request_uri']}/edit?id=" paramId="id" paramProperty="id"
                    value="Edit" media=""></display:column>
    <display:column href="${requestScope['javax.servlet.forward.request_uri']}/delete?id=" paramId="id"
                    paramProperty="id" value="Delete" media=""/>
</display:table>

<%--

<table style="border: 1px solid; width: 500px; text-align:center">
    <thead style="background:#fcf">
    <tr>
        <th>User ID</th>
        <th>Name</th>
        <th>Age</th>
        <th>admin rights</th>
        <th>Date of creation</th>
        <th colspan="3"></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${users}" var="user">
        <c:url var="editUrl" value="//users/edit?id=${user.id}"/>
        <c:url var="deleteUrl" value="//users/delete?id=${user.id}"/>
        <tr>
            <td><c:out value="${user.id}"/></td>
            <td><c:out value="${user.name}"/></td>
            <td><c:out value="${user.age}"/></td>
            <td><c:out value="${user.isAdmin}"/></td>
            <td><c:out value="${user.createdDate}"/></td>
            <td><a href="${editUrl}">Edit</a></td>
            <td><a href="${deleteUrl}">Delete</a></td>
            <td><a href="${addUrl}">Add</a></td>
        </tr>
    </c:forEach>
    </tbody>
</table>

--%>

<c:if test="${empty users}">
    There are currently no users in the list. <a href="${addUrl}">Add</a> a person.
</c:if>

<c:url var="addUrl" value="/users/add"/>
<form action="${addUrl}">
    <input type="submit" value="Add new user">
</form>

</body>
</html>