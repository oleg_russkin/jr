<%--
  Created by IntelliJ IDEA.
  User: rolep
  Date: 23/02/16
  Time: 23:39
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User DB - Adding new user</title>
</head>
<body>

<h1>User Database</h1>

<c:url var="saveUrl" value="/users/add"/>
<form:form modelAttribute="userAttribute" method="POST" action="${saveUrl}">
    <table>
        <tr>
            <td><form:label path="name">User Name:</form:label></td>
            <td><form:input path="name"/></td>
        </tr>

        <tr>
            <td><form:label path="age">Age</form:label></td>
            <td><form:input path="age"/></td>
        </tr>

        <tr>
            <td><form:label path="isAdmin">admin rights</form:label></td>
            <td><form:checkbox path="isAdmin"/></td>
        </tr>
    </table>

    <input type="submit" value="Save"/>
</form:form>

</body>
</html>
