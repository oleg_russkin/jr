<%--
  Created by IntelliJ IDEA.
  User: rolep
  Date: 23/02/16
  Time: 23:39
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User DB - User was deleted</title>
</head>
<body>

<h1>User Database</h1>

<p>User with id ${id} was deleted at <%= new java.util.Date() %>
</p>

<c:url var="mainUrl" value="/users"/>
<p>Return to <a href="${mainUrl}">User List</a></p>

</body>
</html>